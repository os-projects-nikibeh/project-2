#include "FileOpener.hpp"

vector<vector<long double> > getClassifierWeightVectors(string classifierFileName)
{
	vector<vector<long double> > weightVectors;
	ifstream inputFile;
	inputFile.open(classifierFileName.c_str());
	string line, part;
	getline(inputFile, line);
	while(getline(inputFile, line))
	{
		vector<long double> wVector;
		for(int i = 0; i < line.length(); i++)
		{
			if(line[i] != ',')
				part += line[i];
				
			else
			{
				long double dpart = stod(part);
				wVector.push_back(dpart);
				part = "";
			}
		}

		if(part.length() != 0)
		{
			long double dpart = stod(part);
			wVector.push_back(dpart);
			part = "";
		}

		weightVectors.push_back(wVector);
	}

	inputFile.close();
	return weightVectors;
}

vector<vector<long double> > getData(string datasetFileName)
{
	vector<vector<long double> > dataVectors;
	ifstream inputFile;
	inputFile.open(datasetFileName.c_str());
	string line, part;
	getline(inputFile, line);
	while(getline(inputFile, line))
	{
		vector<long double> dVector;
		for(int i = 0; i < line.length(); i++)
		{
			if(line[i] != ',')
				part += line[i];
			else
			{
				long double dpart = stod(part);
				dVector.push_back(dpart);
				part = "";
			}
		}

		if(part.length() != 0)
		{
			long double dpart = stod(part);
			dVector.push_back(dpart);
			part = "";
		}

		dataVectors.push_back(dVector);
	}

	inputFile.close();
	return dataVectors;
	
}

vector<int> getLabels(string labelsFileName)
{
	vector<int> labelsVector;
	ifstream inputFile;
	inputFile.open(labelsFileName.c_str());
	string line;
	getline(inputFile, line);
	while(getline(inputFile, line))
	{
		int iline = atoi(line.c_str());
		labelsVector.push_back(iline);
	}

	inputFile.close();
	return labelsVector;
}