#ifndef ENSEMBLECLASSIFIER_HPP
#define ENSEMBLECLASSIFIER_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>

#define READ_END 0
#define WRITE_END 1

using namespace std;

vector<string> getClassifiersNames(char* dir);
vector<int> getDataVotes(string dataVotesStr);
double compare(vector<int> dataVotes, vector<int> labels);

#endif