#ifndef FILEOPENER_HPP
#define FILEOPENER_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <fcntl.h>

using namespace std;

vector<vector<long double> > getClassifierWeightVectors(string classifierFileName);
vector<vector<long double> > getData(string datasetFileName);
vector<int> getLabels(string labelsFileName);

#endif