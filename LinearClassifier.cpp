#include "LinearClassifier.hpp"
#include "FileOpener.hpp"

int chooseClass(vector<vector<long double> > weightVectors, vector<long double> dVector)
{
	long double maxScore = 0;
	for(int j = 0; j < dVector.size(); j++)
		maxScore += weightVectors[0][j] * dVector[j];
	
	maxScore += weightVectors[0][dVector.size()];

	int chosenClass = 0;

	for(int i = 1; i < weightVectors.size(); i++)
	{
		long double score = 0;
		for(int j = 0; j < dVector.size(); j++)
			score += weightVectors[i][j] * dVector[j]; 
		
		score += weightVectors[i][dVector.size()];
		
		if(score > maxScore)
		{
			maxScore = score;
			chosenClass = i;
		}
		
	}

	return chosenClass;
}

int main(int argc, char* argv[])
{
	string classifierFileName = argv[1];
	string addr(argv[2]);
	string datasetFileName = addr + "/dataset.csv";
	vector<vector<long double> > weightVectors;
	vector<vector<long double> > dataVectors;
	vector<int> dataClasses;
	weightVectors = getClassifierWeightVectors(classifierFileName);
	dataVectors = getData(datasetFileName);

	for(int i = 0; i < dataVectors.size(); i++)
	{
		int chosenClass = chooseClass(weightVectors, dataVectors[i]);
		dataClasses.push_back(chosenClass);
	}

	/* convert vector to string for sending to voter */
	string dataClassesStr;
	dataClassesStr += to_string(weightVectors.size()) + "@";
	for(int i = 0; i < dataClasses.size(); i++)
		dataClassesStr += to_string(dataClasses[i]) + " ";
	dataClassesStr += '\n';
	
	/* send dataClasses to voter */
	int fd = open(argv[3], O_WRONLY);
	write(fd, dataClassesStr.c_str(), dataClassesStr.length());
	close(fd);

	return 0;
}