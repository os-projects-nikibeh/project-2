#include "EnsembleClassifier.hpp"
#include "FileOpener.hpp"

vector<string> getClassifiersNames(char* dir)
{
	vector<string> classifierFilesNames;
	string dirStr(dir);
	DIR *directory;
	struct dirent *dent;
	directory = opendir(dir);
	if(directory != NULL)
	{
		while((dent = readdir(directory)) != NULL)
		{
			if(dent->d_name[0] != '.' && dent->d_name[1] != '.')
				classifierFilesNames.push_back(dirStr + '/' + dent->d_name);
				
		}
		closedir(directory);
	}
	else
		cout << "Cannot open the directory!\n";
	

	return classifierFilesNames;
}

vector<int> getDataVotes(string dataVotesStr)
{
	vector<int> dataVotes;
	string part;

	for(int i = 0; i < dataVotesStr.length(); i++)
	{
		if(dataVotesStr[i] != ' ' && dataVotesStr[i] != '\n')
			part += dataVotesStr[i];
		
		else
		{
			int ipart = atoi(part.c_str());
			dataVotes.push_back(ipart);
			part = "";
		}
	}

	return dataVotes;
}

double compare(vector<int> dataVotes, vector<int> labels)
{
	double correctCount = 0;
	double accuracy;
	for(int i = 0; i < dataVotes.size(); i++)
	{
		if(dataVotes[i] == labels[i])
			correctCount++;
	}
	accuracy = ((correctCount / dataVotes.size()) * 100);
	return accuracy;
}

int main(int argc, char* argv[])
{
	vector<string> classifierFilesNames;

	if(argc != 3)
	{
		cout << "Not enough arguments!\n";
		return 0;
	}

	classifierFilesNames = getClassifiersNames(argv[2]);
	if(classifierFilesNames.size() == 0)
		return 0;

	int classifierNum = classifierFilesNames.size();
	char* voterPipeDir = (char*)"voterFifo";
	char* mainPipeDir = (char*)"mainFifo";

	/* creating linearClassifiers */
	char* linearDir = (char*)"./LinearClassifier.out";
	int fd[classifierNum][2];
	for(int i = 0; i < classifierNum; i++)
	{
		if(pipe(fd[i]) == -1)
		{
			cout << "Creating pipe failed!\n";
			return 0;
		}

		int pid = fork();

		if(pid < 0)	//fork failed
		{
			cout << "Fork failed!\n";
			return 0;
		}

		if(pid > 0)	//parent process	
		{
			close(fd[i][READ_END]);
			string message = classifierFilesNames[i];
			write(fd[i][WRITE_END], message.c_str(), message.length());
			close(fd[i][WRITE_END]);
		}

		else	//child process
		{
			close(fd[i][WRITE_END]);
			char message[500];
			read(fd[i][READ_END], message, 500);
			close(fd[i][READ_END]);

			string voterPipeName = voterPipeDir + to_string(i);

			int pipeFd = open(voterPipeName.c_str(), O_WRONLY | O_CREAT, 0666);
    		close(pipeFd);
	
			mkfifo(voterPipeName.c_str(), 0666);

			char* args[] = {linearDir ,message, argv[1], (char*)voterPipeName.c_str(), NULL};
			execvp(linearDir, args);
		}
	}

	for(int i = 0; i < classifierNum; i++)
		wait(NULL);

	/* creating voter */
	char* voterDir = (char*)"./Voter.out";
	int voterPid = fork();
	if(voterPid < 0)
	{
		cout << "Fork failed!\n";
		return 0;
	}

	if(voterPid > 0)	//parent process
	{
		wait(NULL);
		ifstream mainFd;
		mainFd.open(mainPipeDir);
		string dataVotesStr;
		getline(mainFd, dataVotesStr);
		vector<int> dataVotes = getDataVotes(dataVotesStr);
		string addr(argv[1]);
		string labelsFileName = addr + "/labels.csv";
		vector<int> labels = getLabels(labelsFileName);
		double accuracy = compare(dataVotes, labels);
		cout.setf(ios::fixed, ios::floatfield);
		cout.precision(2);
		cout << "Accuracy: " << accuracy << "%" << endl;
		mainFd.close();
		
	}
	else	//child process
	{
		int mainPipeFd = open(mainPipeDir, O_WRONLY | O_CREAT, 0666);
	    close(mainPipeFd);
		mkfifo(mainPipeDir, 0666);
		string classifierNumStr = to_string(classifierNum);
		char* voterArgs[] = {voterDir, (char*)classifierNumStr.c_str(), NULL};
		execvp(voterDir, voterArgs);
	}

	/* deleting named pipes */
	for(int i = 0; i < classifierNum; i++)
	{
		string pipeName = voterPipeDir + to_string(i);
		unlink(pipeName.c_str());
	}

	unlink(mainPipeDir);


	return 0;
}