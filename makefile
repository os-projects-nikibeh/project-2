all: EnsembleClassifier.out LinearClassifier.out Voter.out

EnsembleClassifier.out: FileOpener.o EnsembleClassifier.o 
		g++ -std=c++11 FileOpener.o EnsembleClassifier.o -o EnsembleClassifier.out

EnsembleClassifier.o: EnsembleClassifier.cpp EnsembleClassifier.hpp 
		g++ -std=c++11 -c EnsembleClassifier.cpp

FileOpener.o: FileOpener.cpp FileOpener.hpp
		g++ -std=c++11 -c FileOpener.cpp

LinearClassifier.out: FileOpener.o LinearClassifier.o
		g++ -std=c++11 FileOpener.o LinearClassifier.o -o LinearClassifier.out

LinearClassifier.o: LinearClassifier.cpp LinearClassifier.hpp FileOpener.hpp
		g++ -std=c++11 -c LinearClassifier.cpp

Voter.out: Voter.o
		g++ -std=c++11 Voter.o -o Voter.out

Voter.o: Voter.cpp Voter.hpp
		g++ -std=c++11 -c Voter.cpp

clean:
		rm *.o
		rm *.out