#ifndef LINEARCLASSIFIER_HPP
#define LINEARCLASSIFIER_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>

using namespace std;

int chooseClass(vector<vector<int> > weightVectors, vector<int> dVector);

#endif