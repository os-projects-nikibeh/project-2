#ifndef VOTER_HPP
#define VOTER_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <fcntl.h>

using namespace std;

int getNumberOfClasses(string dataClassesStr);
vector<int> getClassifierDataClasses(string dataClassesStr);
int vote(vector<vector<int> > allClassifiersDataClasses, int classifierNum, int d);

#endif