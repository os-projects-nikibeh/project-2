#include "Voter.hpp"

int getNumberOfClasses(string dataClassesStr)
{
	string numOfClassesStr = dataClassesStr.substr(0, dataClassesStr.find('@'));
	int numOfClasses = atoi(numOfClassesStr.c_str());
	return numOfClasses;
}

vector<int> getClassifierDataClasses(string dataClassesStr)
{
	vector<int> classifierDataClasses;
	string part;
	
	dataClassesStr = dataClassesStr.substr(dataClassesStr.find('@') + 1);
	for(int i = 0; i < dataClassesStr.length(); i++)
	{
		if(dataClassesStr[i] != ' ' && dataClassesStr[i] != '\n')
			part += dataClassesStr[i];
		
		else
		{
			int ipart = atoi(part.c_str());
			classifierDataClasses.push_back(ipart);
			part = "";
		}
	}

	return classifierDataClasses;
}

int vote(vector<vector<int> > allClassifiersDataClasses, int numOfClasses, int d)
{
	int max = -1;
	int dataClass;
	for(int i = 0; i < numOfClasses; i++)
	{
		int count = 0;
		for(int j = 0; j < allClassifiersDataClasses.size(); j++)
		{
			if(allClassifiersDataClasses[j][d] == i)
				count++;
			if(count > max)
			{
				max = count;
				dataClass = i;
			}
		}
	}
	
	return dataClass;

}

int main(int argc, char* argv[])
{
	char* voterPipeDir = (char*)"voterFifo";
	char* mainPipeDir = (char*)"mainFifo";
	string dataClassesStr;
	int numOfClasses;
	int classifierNum = atoi(argv[1]);
	vector<vector<int> > allClassifiersDataClasses;

	/* get dataClasses from linearClassifiers */
	for(int i = 0; i < classifierNum; i++)
	{
		string voterPipeName = voterPipeDir + to_string(i);
		ifstream fd;
		fd.open(voterPipeName);
		getline(fd, dataClassesStr);
		numOfClasses = getNumberOfClasses(dataClassesStr);
		vector<int> classifierDataClasses = getClassifierDataClasses(dataClassesStr);
		allClassifiersDataClasses.push_back(classifierDataClasses);

		fd.close();
	}

	int numOfData = allClassifiersDataClasses[0].size();
	int dataVotes[numOfData];
	for(int d = 0; d < numOfData; d++)
		dataVotes[d] = vote(allClassifiersDataClasses, numOfClasses, d);

	/* convert to string for sending to ensemble */
	string dataVotesStr;
	for(int i = 0; i < numOfData; i++)
		dataVotesStr += to_string(dataVotes[i]) + " ";
	dataVotesStr += '\n';

	/* send dataVotes to ensembler */
	int mainFd = open(mainPipeDir, O_WRONLY);
	write(mainFd, dataVotesStr.c_str(), dataVotesStr.length());
	close(mainFd);

	return 0;
}